import sys

alphabet_b = [chr(i) for i in range(ord('А'), ord('Я') + 1)]				#Формирование списка с кодами букв алфавита

def Encr(mes,shift=0,t=""):			#Функция шифрования
	for symb in mes:
		if symb in alphabet_b:													#Для каждого символа в тексте находится его индекс в списке с кодами, 
			t+=alphabet_b[int(alphabet_b.index(symb)+shift)%len(alphabet_b)]	#прибавляется смещение, ищется новый индекс и записывается в строку шифртекста
			shift+=1
			if shift==32 :
				shift=0		
		else:
			t+=symb
	return t     
	
def Decr(mes,shift=0,t=""):			#Функция расшифрования
	for symb in mes:
		if symb in alphabet_b:													#Для каждого символа в тексте находится его индекс в списке с кодами, 
			t+=alphabet_b[int(alphabet_b.index(symb)-shift)%len(alphabet_b)]	#вычитается смещение, ищется новый индекс и записывается в строку открытого текста
			shift+=1
			if shift==32 :
				shift=0		
		else:
			t+=symb
	return t     

def input_text(choise):					#Функция, считывающая открытый текст и шифртекст из файлов
	if choise =="1":
		filename = "./Текст"
	if choise =="2":
		filename = "./Шифртекст"
	with open(filename+".txt", "r",encoding="utf-8") as openfile:
		mess = openfile.read().upper()
	return mess

def out_cipher(cipher):					#Функция вывода шифртекста в файл
	with open("Шифртекст.txt", "w",encoding="utf-8") as openfile:
		openfile.write(cipher)
	

while 1:
	choise=input("[0] : Выход\n[1] : Шифрование\n[2] : Расшифрование\n Выбор - ")	#Определяет меню выбора				
	if choise=="1":				#Шифрование	
		encdata=Encr(input_text(choise))
		out_cipher(encdata)
		print("Зашифрованный текст = \n",encdata)
	elif choise=="2":						#Расшифрование
		decrdata=Decr(input_text(choise))
		print("Расшифрованный текст = \n",decrdata)
	elif choise == "0":						#Выход
		sys.exit(0)
	else:									#Неправильный ввод пункта меню
		print("Выберите варианты меню")
		continue


