import re
import copy
import sys 
reg_x_length = 19
reg_y_length = 22
reg_z_length = 23
reg_r_length = 17

key_one = ""
reg_x = []
reg_y = []
reg_z = []
reg_r = []

def loading_registers(key): # Инициализирует регистры, используя ключ как параметр
	i = 0
	while(i < reg_x_length): 
		reg_x.insert(i, int(key[i])) # Берётся 19 первых элементов ключа
		i = i + 1
	j = 0
	p = reg_x_length
	while(j < reg_y_length): 
		reg_y.insert(j,int(key[p])) # Берутся следующие 22 элемента ключа
		p = p + 1
		j = j + 1
	k = reg_y_length + reg_x_length
	r = 0
	while(r < reg_z_length): 
		reg_z.insert(r,int(key[k])) # Берутся следующие 23 элемента ключа
		k = k + 1
		r = r + 1
	q=0
	w = reg_y_length + reg_x_length+ reg_z_length
	while(q < reg_r_length): 
		reg_r.insert(q,int(key[w])) # Берутся оставшиеся 17 элементов ключа
		q += 1
		w += 1

def set_key(key): # Определяет ключ, повторная проверка на длину и допустимые символы ключа, запуск инициализации РСЛОС   
	if(len(key) == 81 and re.match("^([01])+", key)):
		key_one=key
		loading_registers(key)
		return True
	return False

def get_majority(x,y,z):  # Возвращает значение обратной функции F для заданных бит регистров на каждом такте
	return (x&y | x&z | y&z)

def get_keystream(length): # Функция вычисления двоичной ключевой последовательности из регистров ключа
	reg_x_temp = copy.deepcopy(reg_x)
	reg_y_temp = copy.deepcopy(reg_y)
	reg_z_temp = copy.deepcopy(reg_z)
	reg_r_temp = copy.deepcopy(reg_r)
	
	reg_r_temp[2], reg_r_temp[6], reg_r_temp[9] = 1,1,1

	keystream = ""
	i = 0
	while i < length:
		majority = get_majority(reg_r_temp[2], reg_r_temp[6], reg_r_temp[9])
		
		if reg_r_temp[9] == majority: 
			new = reg_x_temp[13] ^ reg_x_temp[16] ^ reg_x_temp[17] ^ reg_x_temp[18]
			reg_x_temp_two = copy.deepcopy(reg_x_temp)
			j = 1
			while(j < len(reg_x_temp)):
				reg_x_temp[j] = reg_x_temp_two[j-1]
				j = j + 1
			reg_x_temp[0] = new

		if reg_r_temp[2] == majority:
			new_one = reg_y_temp[20] ^ reg_y_temp[21]
			reg_y_temp_two = copy.deepcopy(reg_y_temp)
			k = 1
			while(k < len(reg_y_temp)):
				reg_y_temp[k] = reg_y_temp_two[k-1]
				k = k + 1
			reg_y_temp[0] = new_one

		if reg_r_temp[6] == majority:
			new_two = reg_z_temp[7] ^ reg_z_temp[20] ^ reg_z_temp[21] ^ reg_z_temp[22]
			reg_z_temp_two = copy.deepcopy(reg_z_temp)
			m = 1
			while(m < len(reg_z_temp)):
				reg_z_temp[m] = reg_z_temp_two[m-1]
				m = m + 1
			reg_z_temp[0] = new_two
			
		new_three = reg_r_temp[11] ^ reg_r_temp[16]
		reg_r_temp_two = copy.deepcopy(reg_r_temp)
		l = 1
		while(l < len(reg_r_temp)):
			reg_r_temp[l] = reg_r_temp_two[l-1]
			l = l + 1
		reg_r_temp[0] = new_three
		
		major_x=get_majority(reg_x_temp[11], reg_x_temp[13], reg_x_temp[14])
		major_y=get_majority(reg_y_temp[8], reg_y_temp[12], reg_y_temp[15])
		major_z=get_majority(reg_y_temp[12], reg_y_temp[15], reg_y_temp[17])

		keystream += str(reg_x_temp[18] ^ reg_y_temp[21] ^ reg_z_temp[22] ^ major_x ^ major_y ^ major_z)
		i = i + 1
	return keystream

	
def to_binary(plain): # Перевод текста в двоичный код
	binary = list(map(lambda x: "{0:b}".format(ord(x)).zfill(11), plain))
	return binary
	

def convert_binary_to_str(binary): # Перевод двоичного кода в текст
	s = "".join(map(lambda x: chr(int(x,2)), binary))
	return str(s)

def encrypt_decrypt(plain): 
	s = []									# Основная функция, выполнящая как шифрование, так и расшифрование:
	nov_kod=""								# Берётся открытый текст, переводится в двоичный код, подгружается генерируемая РСЛОС двоичная ключевая последовательность
	binary = to_binary(plain)				# Двоичный код текста XOR'ится (складывается по модулю 2)с ключевой последовательностью
	col_simv=len(binary)*11
	keystream = get_keystream(col_simv)
	i=0
	for kod_simv in binary:
		for ind_simv in range(len(kod_simv)):
			nov_kod += str(int(kod_simv[ind_simv]) ^ int(keystream[i]))
			i += 1
		s.append(nov_kod)
		nov_kod=""
	shifr=convert_binary_to_str(s)
	return shifr

def user_input_key(): 				# Функция ввода ключа с проверкой на правильность ввода 
	tha_key = str(input('Enter a 81-bit key: '))
	if (len(tha_key) == 81 and re.match("^([01])+", tha_key)):
		return tha_key
	else:
		while(len(tha_key) != 81 and not re.match("^([01])+", tha_key)):
			if (len(tha_key) == 81 and re.match("^([01])+", tha_key)):
				return tha_key
			tha_key = str(input('Enter a 81-bit key: '))
	return tha_key

def user_input_choice(): 			# Функция, определяющая выбор пользователя
	someIn = str(input('[0]: Quit\n[1]: Encrypt\n[2]: Decrypt\nPress 0, 1, or 2: '))
	if (someIn == '0' or someIn == '1' or someIn == '2'):
		return someIn
	else:
		while(someIn != '0' or someIn != '1' or someIn != '2'):
			if (someIn == '0' or someIn == '1' or someIn == '2'):
				return someIn
			someIn = str(input('[0]: Quit\n[1]: Encrypt\n[2]: Decrypt\nPress 0, 1, or 2: '))
	return someIn

def user_input_text(choice): # Функция ввода открытого текста и шифртекста из файлов
	if choice=="1":
		filename = "./Текст"
	else:
		filename = "./Шифртекст"
	handle = open(filename+".txt", "r",encoding='utf-8')
	data = handle.read()
	handle.close()		
	return data

def crypt_io_ciphertext(first_choice,text):		#Функция вывода шифртекста и расшифрованного текста
	if first_choice=="1":
		decrypt_file = open("./Шифртекст.txt", "w",encoding="utf-8")
		decrypt_file.write(text)
		decrypt_file.close()	
	if first_choice=="2":
		decrypt_file = open("./Расшифрованный текст.txt", "w",encoding="utf-8")
		decrypt_file.write(text)
		decrypt_file.close()
	
	
def the_main(): #Меню с выбором возможных действий программы 
	key = str(user_input_key())
	set_key(key)
	while(1):
		first_choice = user_input_choice()
		if(first_choice == '0'):					# Выход
			print('Have an awesome day!!!')
			sys.exit(0)
		elif(first_choice == '1'):					# Шифрование
			text = user_input_text(first_choice)
			print("Исходный текст = ",text,end="\n")
			cipher=encrypt_decrypt(text)
			print("Зашифрованный текст = ",cipher,end="\n\n\n")
			crypt_io_ciphertext(first_choice,cipher)
		elif(first_choice == '2'):					# Расшифрование
			ciphertext = user_input_text(first_choice)
			print("Зашифрованный текст = ",ciphertext,end="\n")
			decrypt=encrypt_decrypt(ciphertext)
			print("Расшифрованный текст = ",decrypt,end="\n\n\n")
			crypt_io_ciphertext(first_choice,decrypt)
			

#Example of 81-bit key: 010100100001101011000111000110010010100100000011011111101011011100101100101001111

the_main()
