from base64 import b64encode, b64decode
import hashlib
from Cryptodome.Cipher import AES
import os
from Cryptodome.Random import get_random_bytes
import sys
from Cryptodome.Util.Padding import pad,unpad

 
def input_open_text():					# Ввод открытого текста
	filename = "./Текст"
	with open(filename+".txt", "r",encoding="utf-8") as openfile:
		startMessage = openfile.read().encode()
	return startMessage
	
def input_cipher_file(): 				# Ввод зашифрованного сообщения, ключа и инициализирующего вектора
	with open("Шифртекст.txt", "r",encoding="utf-8") as cipherfile:
		shifr = cipherfile.read()
	with open("Ключ AES.txt", "r",encoding="utf-8") as openfile:
		key_iv = openfile.read().split("\n")
		return shifr,key_iv[0],key_iv[1]
 
def out_cipher(cipher,key,iv):			# Вывод шифртекста, ключа и инициализирующего вектора в файлы
	vivod_key = key + "\n" + iv
	with open("Шифртекст.txt", "w",encoding="utf-8") as cipherfile:
		cipherfile.write(cipher)
	with open("Ключ AES.txt", "w",encoding="utf-8") as keyfile:
		keyfile.write(vivod_key)
 
def out_rashifr(text):					# Вывод расшифрованного текста в файл
	with open("Расшифрованный текст.txt", "w",encoding="utf-8") as textfile:
		textfile.write(text)

 
# Функция шифрования
def encrypt(data):						
	key = get_random_bytes(16)				#Генерация случайного ключа (можно изменить, 16 байт-24 байта-32 байта)
	cipher = AES.new(key, AES.MODE_CBC)		# Инициализация настроек шифрования
	ct_bytes = cipher.encrypt(pad(data, AES.block_size))		# Шифрование с дополнением последнего блока сообщения, если он не полный
	iv = b64encode(cipher.iv).decode('utf-8')				
	ct = b64encode(ct_bytes).decode('utf-8')			# Форматирование для вывода 
	key = b64encode(key).decode('utf-8')
	return ct, iv, key

# Функция расшифрования
def decrypt(ct,iv,key):
	try:
		iv = b64decode(iv)
		ct = b64decode(ct)				# Форматирование значений шифра,ключа и iv для расшифрования
		key = b64decode(key)
		cipher = AES.new(key, AES.MODE_CBC, iv)		# Расшифрование
		pt = unpad(cipher.decrypt(ct), AES.block_size)
		return pt.decode()
	except ValueError or KeyError:
		print("Incorrect decryption")



def main():
	while 1:
		choise=input("[0] : Выход\n[1] : Шифрование\n[2] : Расшифрование\n Выбор - ")			# Меню выбора процедур
		if choise == "1":	# Шифрование
			cipher,inV,key = encrypt(input_open_text())
			print("Зашифрованный текст = \n", cipher,end="\n\n")
			out_cipher(cipher,inV,key)
		elif choise == "2" :	# Расшифрование
			decr_shifr, decr_key, decr_iv =input_cipher_file()
			decrypted = decrypt(decr_shifr, decr_key, decr_iv)
			print("Расшифрованный текст \n", decrypted, end="\n\n")
			out_rashifr(decrypted)
		elif choise == "0":		# Выход
			sys.exit(0)
		else:					# Неверный вариант 
			print("Выберите варианты меню")
			continue

main()
