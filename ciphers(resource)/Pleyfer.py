cryptMode = input("[E]ncrypt|[D]ecrypt: ").upper()			# Ввод режима шифрования с проверкой
if cryptMode not in ['E','D']:
	print("Error: mode is not Found!"); raise SystemExit
		
alphabet="АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ,.?!\":;%@- 0123456789"			# Задание исходного алфавита 
vvod=input("Введите пароль без повторяющихся букв - ").upper()				# Ввод пароля
for symbol in vvod:
    if symbol in alphabet:								#Удаление символов ключа из исходного алфавита
        alphabet=alphabet.replace(symbol,"")

matrixKey = [["*"] * 6 for i in range(9)]				# Формирование массива для алфавита(нужно для операций шифра) 

per=0

for bukv in vvod:
	for stroke in range(len(matrixKey)):
		for ind in range(len(matrixKey[stroke])):
			if matrixKey[stroke][ind] not in vvod:			# Заполнение массива первых позиций алфавита символами ключа
				matrixKey[stroke][ind]=bukv
				per=0
				break
			per=1
		if per==1:
			continue
		else:
			break
			
for symb in alphabet:									# Дозаполнение массива оставшимися символами из алфавита
	for stroke in range(len(matrixKey)):
		for ind in range(len(matrixKey[stroke])):
			if matrixKey[stroke][ind] not in alphabet and matrixKey[stroke][ind] not in vvod:
				matrixKey[stroke][ind]=symb
				per=0
				break
			per=1
		if per==1:
			continue
		else:
			break
			

addSymbol = '*'

def two_sim(mes,binaryList=[],k=""):				# Разбиение введённого текста на биграммы
	binaryList.clear()
	for i in mes:
		k += i
		if len(k) == 2:
			binaryList.append(k)
			k = ""
	return binaryList

def encryptDecrypt(mode, message, final = ""):			# Функция, объединяющая и шифрование, и расшифрование (на вход подаётся режим работы)
	if mode == 'E':
		for index in range(1,len(message)):
			if len(message) % 2 != 0:					# Неполная биграммы заполняется символом *
				message+=addSymbol
	binaryList = two_sim(message)
	for binary in range(len(binaryList)):
		binaryList[binary] = list(binaryList[binary])
		for indexString in range(len(matrixKey)):
			for indexSymbol in range(len(matrixKey[indexString])):
				if binaryList[binary][0] == matrixKey[indexString][indexSymbol]:
					y0, x0 = indexString, indexSymbol
				if binaryList[binary][1] == matrixKey[indexString][indexSymbol]:
					y1, x1 = indexString, indexSymbol
		if (y0==y1):
			if mode == 'E':										# операции определения новых значений символов биграммы
				x0 = x0 + 1 if x0 != 5 else 0
				x1 = x1 + 1 if x1 != 5 else 0
			else:
				x0 = x0 - 1 if x0 != 0 else 5
				x1 = x1 - 1 if x1 != 0 else 5
		elif (x0==x1):
			if mode == 'E':
				y0 = y0 + 1 if y0 != 8 else 0
				y1 = y1 + 1 if y1 != 8 else 0
			else:
				y0 = y0 - 1 if y0 != 0 else 8
				y1 = y1 - 1 if y1 != 0 else 8
		else:
			if mode == 'E':
				tmp=x0
				x0=x1
				x1=tmp
			else:
				tmp=x0
				x0 = x1
				x1 = tmp
		binaryList[binary][0] = matrixKey[y0][x0]			#Запись в биграмму новых значений из алфавита
		binaryList[binary][1] = matrixKey[y1][x1]
	for binary in range(len(binaryList)):
		for symbol in binaryList[binary]:					# Добавление результата к выходной строке
			final += symbol
	return final

def input_text(choise):					#Функция, считывающая открытый текст и шифртекст из файлов
	if choise =="E":
		with open("Текст.txt", "r",encoding="utf-8") as openfile:
			mess = openfile.read().replace("\n"," ").upper()
		return mess		
	if choise =="D":
		with open("Шифртекст.txt", "r",encoding="utf-8") as openfile:
			mess = openfile.read()
		return mess
		
def crypt_io(choice,text):		#Функция вывода шифртекста и расшифрованного текста
	if choice=="E":
		decrypt_file = open("./Шифртекст.txt", "w",encoding="utf-8")
		decrypt_file.write(text)
		decrypt_file.close()	
	if choice=="D":
		decrypt_file = open("./Расшифрованный текст.txt", "w",encoding="utf-8")
		decrypt_file.write(text)
		decrypt_file.close()	
		
if cryptMode == "E":				#Шифрование
	mess = input_text(cryptMode)
	encdata=encryptDecrypt(cryptMode,mess)
	crypt_io(cryptMode,encdata)
	print("Зашифрованный текст = ",encdata)
else:								#Расшифрование
	shifr = input_text(cryptMode) 
	decrdata =encryptDecrypt(cryptMode,shifr)
	crypt_io(cryptMode,decrdata)	
	print("Расшифрованный текст = ",decrdata)
