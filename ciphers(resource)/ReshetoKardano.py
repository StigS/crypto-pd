import copy
import random

def input_text(choise):					#Функция, считывающая открытый текст и шифртекст из файлов
	if choise =="1":
		filename = "./Текст"
	if choise =="2":
		filename = "./Шифртекст"
	with open(filename+".txt", "r",encoding="utf-8") as openfile:
		mess = openfile.read().replace("\n"," ")
	if (len(mess)%lenght_grid) != 0:							
		for i in range(lenght_grid -(len(mess)%lenght_grid)):	#Заполнение последнего блока текста
			mess += chr(random.randint(1072,1103))				#случайными значениями (если он не полный)	
	return mess

def out_cipher(cipher):					#Функция вывода шифртекста в файл
	with open("Шифртекст.txt", "w",encoding="utf-8") as openfile:
		openfile.write(cipher)


def split_text(data):						# Функция разделение текста на блоки по 48 символов(размер решётки)
	text=[]
	lenght_block=len(data)//(rows*cols)
	for i in range(lenght_block):
		text.append(data[(i*lenght_grid):(lenght_grid*(i+1))])
	return text
		
		
def Encr(mess):				#Функция шифрования
	shifr=''
	for c in mess:						#Для каждого блока текста выполняются перестановки в соответствии с 
		my_list = []					# 4 положениями решётки
		for i in range(rows):
			my_list.append([])
			for j in range(cols):
				my_list[i].append(0)
			
		char_count = 0
		for i in range(rows):
			for j in range(cols):
				if grid_step1[i][j]==1:
					my_list[i][j] = c[char_count]
					char_count += 1


		for i in range(rows):
			for j in range(cols):
				if grid_step2[i][j]==1:
					my_list[i][j] = c[char_count]
					char_count += 1


		for i in range(rows):
			for j in range(cols):
				if grid_step3[i][j]==1:
					my_list[i][j] = c[char_count]
					char_count += 1


		for i in range(rows):
			for j in range(cols):
				if grid_step4[i][j]==1:
					my_list[i][j] = c[char_count]
					char_count += 1
		
		for i in range(rows):
			for j in range(cols):
				shifr += my_list[i][j]		
	return shifr


def Decr(shifr):					#Функция расшифрования
	open_text=''
	for c in shifr:					# Для каждого блока текста алгоритм "собирает" обратно символы
		for i in range(rows):		# в соответствии с каждым возможным положением решётки
			for j in range(cols):
				if grid_step1[i][j] == 1:
					open_text+=c[i*cols+j]


		for i in range(rows):
			for j in range(cols):
				if grid_step2[i][j] == 1:
					open_text+=c[i*cols+j]


		for i in range(rows):
			for j in range(cols):
				if grid_step3[i][j] == 1:
					open_text+=c[i*cols+j]

		for i in range(rows):
			for j in range(cols):
				if grid_step4[i][j] == 1:
					open_text+=c[i*cols+j]
	return open_text



	# Решётку можно менять, но количество как строк, так и столбцов должно быть чётное
	# По умолчанию, я использую таблицу 6х8. Составлял сам
grid_step1 = [[0, 1, 0, 0, 0, 0, 0, 1],
		[1, 0, 0, 0, 1, 0, 1, 0],
		[0, 1, 0, 0, 0, 1, 0, 1],
		[0, 0, 0, 1, 0, 0, 0, 0],
		[0, 0, 1, 0, 0, 0, 0, 0],
		[0, 0, 1, 1, 0, 0, 0, 0]]

tmp_grid = copy.deepcopy(grid_step1)
for i in range(len(tmp_grid)):
	tmp_grid[i].reverse() # 

grid_step2 = copy.deepcopy(tmp_grid) 			#Формирование оставшихся 3 решёток
grid_step3 = copy.deepcopy(grid_step2) 
grid_step3.reverse() 
grid_step4 = copy.deepcopy(grid_step1)
grid_step4.reverse()
		

		
rows = len(grid_step1)
cols = len(grid_step1[0])				#Часто используемые параметры решётки
lenght_grid = rows*cols


while 1:
	choise=input("[0] : Выход\n[1] : Шифрование\n[2] : Расшифрование\n Выбор - ")	#Определяет меню выбора				
	if choise=="1":							#Шифрование	
		encdata=Encr(split_text(input_text(choise)))
		out_cipher(encdata)
		print("Зашифрованный текст = \n",encdata,end="\n\n")
	elif choise=="2":						#Расшифрование
		decrdata=Decr(split_text(input_text(choise)))
		print("Расшифрованный текст = \n",decrdata,end="\n\n")
	elif choise == "0":						#Выход
		sys.exit(0)
	else:									#Неправильный ввод пункта меню
		print("Выберите один из вариантов меню")
		continue



