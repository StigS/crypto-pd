from django.shortcuts import render
from django.http import HttpResponse
from . import Atbash
from . import Cesar
from . import Polibiy

def index(request):
	return render(request, "index.html")

def Alg(request):
	return render(request, "Alg.html")

def atbash(request):
	if request.method == "POST":
		abc = request.POST.get("abc")
		mes = request.POST.get("mes")
		enc = Atbash.Encr(mes, abc)
		#return HttpResponse(enc)
		return render(request, "atbash.html", {"text": enc})
	else:
		return render(request, "atbash.html", {"text": ""})

def cesar(request):
	if request.method == "POST":
		key = int(request.POST.get("key"))
		mes = request.POST.get("mes")
		enc = Cesar.Encr(mes, key)
		dec = Cesar.Decr(mes, key)
		#return HttpResponse(enc)
		return render(request, "cesar.html", {"enc": enc,"dec": dec})
	else:
		return render(request, "cesar.html", {"text": "", "dec": ""})

def polibiy(request):
	if request.method == "POST":
		
		mes = request.POST.get("mes").upper()
		enc = Polibiy.Encr(mes)
		dec = Polibiy.Decr(mes)
		#return HttpResponse(enc)
		return render(request, "polibiy.html", {"enc": enc,"dec": dec})
	else:
		enc = ""
		dec = ""
		return render(request, "polibiy.html", {"text": enc, "dec": dec})

