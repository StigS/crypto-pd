import hashlib
import random
import rsa

def user_input_text(): # Функция ввода текста и подписи
	handle = open("Текст.txt", "r",encoding='utf-8')
	data = handle.read()
	handle.close()		
	return data
	
def user_input_ecp(): # Функция ввода текста и подписи
	handle = open("Подпись.txt", "r",encoding='utf-8')
	ecp = handle.read()
	handle.close()		
	return ecp
	
def user_input_choice(): # Определяет меню выбора возможностей программы
	someIn = str(input('[0]: Выход\n[1]: Вычисление ЭЦП\n[2]: Проверка ЭЦП\n[3]: Генерация ключей\nPress 0, 1, 2 or 3: '))
	if (someIn == '0' or someIn == '1' or someIn == '2' or someIn == '3'):
		return someIn
	else:
		while(someIn != '0' or someIn != '1' or someIn != '2' or someIn == '2'):
			if (someIn == '0' or someIn == '1' or someIn == '2' or someIn == '2'):
				return someIn
			someIn = str(input('[0]: Выход\n[1]: Вычисление ЭЦП\n[2]: Проверка ЭЦП\n[3]: Генерация ключей\nPress 0, 1, 2 or 3: '))
	return someIn
	
def text_hash_sha256(text):		# Функция хеширования (SHA256)
	hash_object = hashlib.sha256(text.encode()) 
	hash_text= hash_object.hexdigest()
	hash_dec = int(hash_text,16)
	print("Хеш сообщения = ",hash_dec)
	return hash_dec


def make_key_pair(length=256):	# Функция, генерирующая открытые и секретные ключи для подписи
	(pubkey, privkey) = rsa.newkeys(length)
	return pubkey, privkey
	
def save_file(filename,key):				# Сохранение параметров ключей в файлы
	key_file = open(filename+".txt", "w")
	if filename=="./Открытые ключи":
		param=str(key.n)+" "+str(key.e)
		key_file.write(str(param))
	elif filename=="./Секретные ключи":
		param=str(key.d)+" "+str(key.p)+" "+str(key.q)
		key_file.write(str(param))
	else:
		key_file.write(str(key))
	key_file.close()

def read_key_file(choise):					# Чтение параметров ключей из файлов
	with open("Открытые ключи.txt", "r",encoding="utf-8") as openfile:
		op = openfile.read().split(" ")
		for i in range(len(op)):
			op[i]=int(op[i])
	if choise =="1":
		with open("Секретные ключи.txt", "r",encoding="utf-8") as openfile:
			priv = openfile.read().split(" ")
			for i in range(len(priv)):
				priv[i]=int(priv[i])
		return op[0],op[1],priv[0],priv[1],priv[2]
	if choise =="2":
		return op[0],op[1]
	
def the_main(): #Основная функция, обрабатывающая ввод пользователя
	while(1):
		first_choice = user_input_choice()
		if(first_choice == '0'):				#Выход
			print('Have an awesome day!!!')
			sys.exit(0)
		elif(first_choice == '1'):				# Вычисление ЭЦП
			text = user_input_text()
			hash = text_hash_sha256(text)
			n,e,d,p,q =  read_key_file(first_choice)
			priv_key = rsa.PrivateKey(n,e,d,p,q)
			ecp = pow(hash,priv_key.d,priv_key.n)
			print("Цифровая подпись = ",ecp)
			save_file("./Подпись",ecp)
			del priv_key
		elif(first_choice == '2'):				# Проверка ЭЦП
			text = user_input_text()
			hash = text_hash_sha256(text)
			ecp = int(user_input_ecp())
			n,e = read_key_file(first_choice)
			pub_key = rsa.PublicKey(n,e)
			rash= pow(ecp,pub_key.e,pub_key.n)
			print("Проверка подписи= ",rash)
			if (hash == rash):
				print("Подпись верна\n\n")
			else:
				print("Подпись неверна")
			del pu
		elif(first_choice == '3'):				# Генерация ключей
			public_key,private_key = make_key_pair()
			print("Открытые ключи:\nN= ",public_key.n,"\nE= ",public_key.e,"\nСекретные ключи:\nD= ",private_key.d,"\nP= ",private_key.p,"\nQ= ",private_key.q)
			save_file("./Открытые ключи",public_key)
			save_file("./Секретные ключи",private_key)


the_main()
