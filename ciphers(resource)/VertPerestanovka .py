import sys

def sort_key(key):				#Сортировка элементов ключа по алфавиту и формирование списка индексов для шифрования
	ind_rev=[]					
	revers=[]
	ind_orig=[]
	for i in key:
			revers.append(ord(i))
	revers.sort()
	for i in range(0,len(revers)):
			revers[i]=chr(revers[i])
	for i in revers:
			ind_rev.append(key.index(i))
	if choise=="1":
		return ind_rev
	if choise=="2":							#Если выбрано "Расшифрование", то формирует обратный список индексов
		for i in key:	
			ind_orig.append(revers.index(i))
		return ind_orig
		

def Encr(text, key):				# Функция шифрования
	n = len(text) 
	m = len(key)
	d = n % m
	if d != 0:
		for i in range(m - d):			#Дополнение текста избыточными символами в случае, когда длина текста 
			text += "-"					#нацело не делится на длину ключа
	n = len(text)
	p = ''
	for x in key:
		q = 0
		while q < n:
			p += ''.join(text[q+x])
			q += m
	return p

def Decr(text, key):			#Функция расшифрования
	n = len(text) 
	m = len(key)
	d=n//m
	dec_str =""
	for q in range(0,d):
		for x in key:
			dec_str += ''.join(text[(d*x+q)])
	return dec_str
	
def input_text(choise):					#Функция, считывающая открытый текст и шифртекст из файлов
	if choise =="1":
		filename = "./Текст"
	if choise =="2":
		filename = "./Шифртекст"
	with open(filename+".txt", "r",encoding="utf-8") as openfile:
		mess = openfile.read()
	return mess

def out_cipher(cipher):					#Функция вывода шифртекста в файл
	with open("Шифртекст.txt", "w",encoding="utf-8") as openfile:
		openfile.write(cipher)


while 1:
	choise=input("[0] : Выход\n[1] : Шифрование\n[2] : Расшифрование\n Выбор - ")	#Определяет меню выбора				
	if choise=="1":							#Шифрование	
		key = input("Введите пароль без повторяющихся букв: ")
		sorted_key = sort_key(key)
		encdata=Encr(input_text(choise),sorted_key)
		out_cipher(encdata)
		print("Зашифрованный текст = \n",encdata,end="\n\n")
	elif choise=="2":						#Расшифрование
		key = input("Введите пароль без повторяющихся букв: ")
		sorted_key = sort_key(key)
		decrdata=Decr(input_text(choise),sorted_key)
		print("Расшифрованный текст = \n",decrdata,end="\n\n")
	elif choise == "0":						#Выход
		sys.exit(0)
	else:									#Неправильный ввод пункта меню
		print("Выберите варианты меню")
		continue
