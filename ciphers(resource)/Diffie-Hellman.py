import sys

def vvod ():												# Запрос к пользователю на ввод общеизвестных a и n 
	n=int(input("Введите общеизвестное n = "))				# с проверкой на требования к этим параметрам
	while n<=2:
		print("n не может быть меньше или равно 2\n")
		n=int(input("Введите общеизвестное n = "))
	a=int(input("Введите общеизвестное a = "))
	while a>=n or a<=1:
		print("а не может быть больше n и не может принимать значения меньше 2 \n")
		a=int(input("Введите общеизвестное a = "))
	return n,a
	
def private_key(n):											# Запрос на ввод личного секретного ключа пользователя
	priv_key=int(input("Введите ваш секретный ключ = "))	# с проверкой на требования к секретному ключу
	while priv_key >= (n-1) or priv_key<2:
		print("Секретный ключ не может быть больше n-1 и меньше 2 ")
		priv_key=int(input("Введите ваш секретный ключ = "))
	return priv_key
		
def public_key(n, a, priv_key):								# Формирование личного открытого ключа пользователя
	return pow(a, priv_key, n)
		
def form_shared_key(pub_key_2, priv_key, n):				# Формирование общего ключа на основе публичного ключа другого пользователя
	return pow(pub_key_2, priv_key, n)						# и секретного ключа формирующего
	
def main():													# Меню с выбором возможностей программы
	n,a = vvod()
	priv_key_1 = private_key(n)
	while 1:
		choise = input ("Введите:\n 1 - для генерации личного открытого ключа\n 2 - для формирования общего ключа\n Любой другой символ - Выход\n Ввод : ")
		if choise=="1":										
			pub_key_1 = public_key(n, a, priv_key_1)
			print("Ваш открытый ключ = ",pub_key_1)
		elif choise=="2":
			pub_key_2 = int(input("Введите открытый ключ другого пользователя = "))
			share = form_shared_key(pub_key_2,priv_key_1,n)
			print("Общий ключ= ", share)
		else:
			sys.exit(0)
		

main()
