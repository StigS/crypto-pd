import re
import copy
import sys 

reg_x_length = 19
reg_y_length = 22
reg_z_length = 23

key_one = ""
reg_x = []
reg_y = []
reg_z = []

def loading_registers(key): # Инициализирует 3 РСЛОС, использует ключ как параметр
	i = 0
	while(i < reg_x_length): 
		reg_x.insert(i, int(key[i])) #Берутся первые 19 элементов ключа
		i = i + 1
	j = 0
	p = reg_x_length
	while(j < reg_y_length): 
		reg_y.insert(j,int(key[p])) #Берутся следующие 22 элемента ключа
		p = p + 1
		j = j + 1
	k = reg_y_length + reg_x_length
	r = 0
	while(r < reg_z_length): 
		reg_z.insert(r,int(key[k])) #Берутся следующие 23 элемента ключа
		k = k + 1
		r = r + 1

def set_key(key): # Определяет ключ, повторная проверка на длину и допустимые символы ключа, запуск инициализации РСЛОС   
	if(len(key) == 64 and re.match("^([01])+", key)):
		key_one=key
		loading_registers(key)
		return True
	return False

def to_binary(plain): # Перевод текста в двоичный код
	binary = list(map(lambda x: "{0:b}".format(ord(x)).zfill(11), plain))
	return binary


def get_majority(x,y,z): # Возвращает значение обратной функции F для заданных бит регистров на каждом такте
	return (x&y | x&z | y&z)

def get_keystream(length): 	# Функция вычисления двоичной ключевой последовательности из регистров ключа
	reg_x_temp = copy.deepcopy(reg_x)
	reg_y_temp = copy.deepcopy(reg_y)
	reg_z_temp = copy.deepcopy(reg_z)
	keystream = []
	i = 0
	while i < length:
		majority = get_majority(reg_x_temp[7], reg_y_temp[9], reg_z_temp[9])
		if reg_x_temp[7] == majority: 
			new = reg_x_temp[13] ^ reg_x_temp[16] ^ reg_x_temp[17] ^ reg_x_temp[18]
			reg_x_temp_two = copy.deepcopy(reg_x_temp)
			j = 1
			while(j < len(reg_x_temp)):
				reg_x_temp[j] = reg_x_temp_two[j-1]
				j = j + 1
			reg_x_temp[0] = new

		if reg_y_temp[9] == majority:
			new_one = reg_y_temp[20] ^ reg_y_temp[21]
			reg_y_temp_two = copy.deepcopy(reg_y_temp)
			k = 1
			while(k < len(reg_y_temp)):
				reg_y_temp[k] = reg_y_temp_two[k-1]
				k = k + 1
			reg_y_temp[0] = new_one

		if reg_z_temp[9] == majority:
			new_two = reg_z_temp[7] ^ reg_z_temp[20] ^ reg_z_temp[21] ^ reg_z_temp[22]
			reg_z_temp_two = copy.deepcopy(reg_z_temp)
			m = 1
			while(m < len(reg_z_temp)):
				reg_z_temp[m] = reg_z_temp_two[m-1]
				m = m + 1
			reg_z_temp[0] = new_two

		keystream.insert(i, reg_x_temp[18] ^ reg_y_temp[21] ^ reg_z_temp[22])
		i = i + 1
	return keystream


def convert_binary_to_str(binary): # Функция перевода из двоичного кода в текст
	s = "".join(map(lambda x: chr(int(x,2)), binary))
	return str(s)

def encrypt_decrypt(plain): 
	s = []									# Основная функция, выполнящая как шифрование, так и расшифрование:
	nov_kod=""								# Берётся открытый текст, переводится в двоичный код, подгружается генерируемая РСЛОС двоичная ключевая последовательность
	binary = to_binary(plain)				# Двоичный код текста XOR'ится (складывается по модулю 2)с ключевой последовательностью
	col_simv=len(binary*11)
	keystream = get_keystream(col_simv)
	i=0
	for kod_simv in binary:
		for ind_simv in range(len(kod_simv)):
			nov_kod += str(int(kod_simv[ind_simv]) ^ keystream[i])
			i += 1
		s.append(nov_kod)
		nov_kod=""
	shifr=convert_binary_to_str(s)
	return shifr

def user_input_key(): 			# Функция ввода ключа с проверкой на правильность ввода 
	tha_key = str(input('Enter a 64-bit key: '))
	if (len(tha_key) == 64 and re.match("^([01])+", tha_key)):
		return tha_key
	else:
		while(len(tha_key) != 64 or not re.match("^([01])+", tha_key)):
			tha_key = str(input('Enter a 64-bit key: '))
			if (len(tha_key) == 64 and re.match("^([01])+", tha_key)):
				return tha_key
			

def user_input_choice(): 			# Функция, определяющая выбор пользователя
	someIn = str(input('[0]: Quit\n[1]: Encrypt\n[2]: Decrypt\nPress 0, 1, or 2: '))
	if (someIn == '0' or someIn == '1' or someIn == '2'):
		return someIn
	else:
		while(someIn != '0' or someIn != '1' or someIn != '2'):
			if (someIn == '0' or someIn == '1' or someIn == '2'):
				return someIn
			someIn = str(input('[0]: Quit\n[1]: Encrypt\n[2]: Decrypt\nPress 0, 1, or 2: '))
	return someIn

def user_input_text(choice): 	# Функция ввода открытого текста и шифртекста
	if choice=="1":
		filename = "./Текст"
	else:
		filename = "./Шифртекст"
	with open(filename+".txt", "r",encoding="utf-8") as openfile:
		data=openfile.read()
	return data

def crypt_io_ciphertext(first_choice,text):		#Функция вывода шифртекста и расшифрованного текста
	if first_choice=="1":
		decrypt_file = open("./Шифртекст.txt", "w",encoding="utf-8")
		decrypt_file.write(text)
		decrypt_file.close()	
	if first_choice=="2":
		decrypt_file = open("./Расшифрованный текст.txt", "w",encoding="utf-8")
		decrypt_file.write(text)
		decrypt_file.close()	

def the_main(): #Меню с выбором возможных действий программы
	key = user_input_key()
	set_key(str(key))
	while(1):
		first_choice = user_input_choice()
		if(first_choice == '0'):	#Выход
			sys.exit(0)
		elif(first_choice == '1'):	#Шифрование
			text = user_input_text(first_choice)
			print("Исходный текст = ",text,end="\n")
			cipher=encrypt_decrypt(text)
			print("Зашифрованный текст = ",cipher,end="\n\n\n")
			crypt_io_ciphertext(first_choice,cipher)
		elif(first_choice == '2'):	#Расшифрование
			ciphertext = user_input_text(first_choice)
			print("Зашифрованный текст = ",ciphertext,end="\n")
			decrypt=encrypt_decrypt(ciphertext)
			print("Расшифрованный текст = ",decrypt,end="\n\n\n")
			crypt_io_ciphertext(first_choice,decrypt)		

#Example of 64-bit key: 0101001000011010110001110001100100101001000000110111111010110111

the_main()
