import sys

lower_cyrillic = [chr(i) for i in range(ord('а'), ord('я') + 1)]		#Формирование списка с цифрами, обозначающими коды букв алфавита(нижний регистр)
upper_cyrillic = [chr(i) for i in range(ord('А'), ord('Я') + 1)]		#Формирование списка с цифрами, обозначающими коды букв алфавита(верхний регистр)

def alphabet(shift):						# Функция формирования смещённого списка с цифрами, обозначающими коды букв алфавита
    return lower_cyrillic[shift:] + \
           lower_cyrillic[:shift] + \
           upper_cyrillic[shift:] + \
           upper_cyrillic[:shift] 
     
def Encr(mes,shift):						# Функция шифрования
    a1 = str(lower_cyrillic + upper_cyrillic)
    a2 = str(alphabet(shift))
    t = mes.translate(str.maketrans(a1,a2))			#Замена символов текста по индексу из исходного списка в смещённый 
    return t     
	
def Decr(mes,shift):						#Функция расшифрования
    a1 = str(lower_cyrillic + upper_cyrillic)
    a2 = str(alphabet(shift))
    t = mes.translate(str.maketrans(a2,a1))			#Замена симмволов текста из смещённого списка 
    return t  										# в исходный по индексу

