alphabet_u= [chr(i) for i in range(ord('А'), ord('Я') + 1)]		#Формирование алфавитов символов верхнего и нижнего регистров
alphabet_d= [chr(i) for i in range(ord('а'), ord('я') + 1)]

def Encr(mes,kluch,t="",i=0):		# Функция шифрования
	for symb in mes:
		if symb in alphabet_u:
			t+=alphabet_u[int((alphabet_u.index(symb)+alphabet_u.index(kluch[i]))%len(alphabet_u))]
			i+=1
			if i==len(kluch) :
				i=0
		elif symb in alphabet_d:
			t+=alphabet_d[int((alphabet_d.index(symb)+alphabet_u.index(kluch[i]))%len(alphabet_d))]
			i+=1
			if i==len(kluch) :
				i=0						
		else:
			t+=symb
	return t  
	
	
def Decr(mes,kluch,t="",i=0):		# Функция расшифрования
	for symb in mes:
		if symb in alphabet_u:
			t+=alphabet_u[int((alphabet_u.index(symb)-alphabet_u.index(kluch[i]))%len(alphabet_u))]
			i+=1
			if i==len(kluch) :
				i=0
		elif symb in alphabet_d:
			t+=alphabet_d[int((alphabet_d.index(symb)-alphabet_u.index(kluch[i]))%len(alphabet_d))]
			i+=1
			if i==len(kluch) :
				i=0						
		else:
			t+=symb
	return t     

	
def input_text(choise):					#Функция, считывающая открытый текст и шифртекст из файлов
	if choise =="1":
		filename = "./Текст"
	if choise =="2":
		filename = "./Шифртекст"
	with open(filename+".txt", "r",encoding="utf-8") as openfile:
		mess = openfile.read()
	return mess.replace("Ё","E").replace("ё","е")

def out_cipher(cipher):					#Функция вывода шифртекста в файл
	with open("Шифртекст.txt", "w",encoding="utf-8") as openfile:
		openfile.write(cipher)

while 1:
	choise=input("[0] : Выход\n[1] : Шифрование\n[2] : Расшифрование\n Выбор - ")	#Определяет меню выбора	
	password=list(input("Введите пароль = ").upper().replace("Ё","Е"))
	if choise=="1":							#Шифрование	
		encdata=Encr(input_text(choise),password)
		out_cipher(encdata)
		print("Зашифрованный текст = \n",encdata,end="\n\n")
	elif choise=="2":						#Расшифрование
		decrdata=Decr(input_text(choise),password)
		print("Расшифрованный текст = \n",decrdata,end="\n\n")
	elif choise == "0":						#Выход
		sys.exit(0)
	else:									#Неправильный ввод пункта меню
		print("Выберите варианты меню")
		continue

