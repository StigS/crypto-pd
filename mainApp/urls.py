from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('Alg/', views.Alg, name='Alg'),
    path('atbash/', views.atbash, name='atbash'),
    path('cesar/', views.cesar, name='cesar'),
    path('polibiy/', views.polibiy, name='polibiy'),
]
