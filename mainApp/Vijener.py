import sys

alphabet_u = [chr(i) for i in range(ord('А'), ord('Я') + 1)]				# Формируются алфавиты верхнего и нижнего регистров
alphabet_d = [chr(i) for i in range(ord('а'), ord('я') + 1)]


# Функция, определяющая гамму для сообщения
def key(mes,t):						
	for symb in mes:
		if symb in alphabet_u or alphabet_d:
			t+=symb	
	return t.upper().replace("\n","").replace(" ","").replace(",","").replace(".","")	\
	.replace("!","").replace("?","").replace("\"","").replace(":","").replace("(","")	\
	.replace(")","").replace("-","").replace(";","")
	
	
# Функция шифрования
def Encr(mes,t="",i=0):
	for symb in mes:
		if symb in alphabet_u:
			t+=alphabet_u[int((alphabet_u.index(symb)+alphabet_u.index(kluch[i]))%len(alphabet_u))]
			i+=1
			if i==len(kluch) :
				i=0		
		elif symb in alphabet_d:
			t+=alphabet_d[int((alphabet_d.index(symb)+alphabet_u.index(kluch[i]))%len(alphabet_d))]
			i+=1
			if i==len(kluch) :
				i=0		
		else:
			t+=symb
	return t  	

# Функция расшифрования	
def Decr(mes,key,t=""):
	for symb in mes:
		if symb in alphabet_u:
			key = alphabet_u[int((alphabet_u.index(symb)-alphabet_u.index(key))%len(alphabet_u))]
			t+=key
			key=key.upper()
		elif symb in alphabet_d:
			key = alphabet_d[int((alphabet_d.index(symb)-alphabet_u.index(key))%len(alphabet_d))]
			t+=key
			key=key.upper()
		else:
			t+=symb
	return t     

	

