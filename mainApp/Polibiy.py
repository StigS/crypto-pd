from re import findall

# Таблица замен
keysPolibiy = {
'А':'11', 'Б':'12', 'В':'13', 'Г':'14',
'Д':'15', 'Е':'16', 'Ж':'21', 'З':'22',
'И':'23', 'Й':'24', 'К':'25', 'Л':'26',
'М':'31', 'Н':'32', 'О':'33', 'П':'34',
'Р':'35', 'С':'36', 'Т':'41', 'У':'42',
'Ф':'43', 'Х':'44', 'Ц':'45', 'Ч':'46',
'Ш':'51', 'Щ':'52', 'Ъ':'53', 'Ы':'54',
'Ь':'55', 'Э':'56', 'Ю':'61', 'Я':'62',
',':'63', '.':'64', ' ':'65', '-':'66',
'?':'71', '!':'72', ';':'73', '(':'74',
')':'75', ':':'76', '\'':'81', '\"':'82',
}

#Поиск в сообщении пар чисел, для расшифрования
def regular(text):
	template = r"[0-9]{2}"
	return findall(template, text)
	
# Функция шифрования
def Encr(message):
	final = []
	for symbol in message:
		if symbol in keysPolibiy:
			final.append(keysPolibiy[symbol])
	return " ".join(final)
	
# Функция расшифрования
def Decr(message):
	final = []
	for twoNumbers in regular(message):
		for key in keysPolibiy:
			if twoNumbers == keysPolibiy[key]:
				final.append(key)
	return "".join(final)





