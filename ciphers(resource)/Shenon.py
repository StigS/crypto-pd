from random import randint		


alphabet="АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ,.?!\": @#$^&-;"
matrixKey = []
for symb in alphabet:
	matrixKey.append(symb)
	

def Encr(text):				#Функция шифрования
	key=''
	gamma=''
	shifr=''
	text = text.upper().replace("Ё","Е").replace("\n"," ")
	print('Открытый текст: \n',text,end="\n\n")
	for symbol in text:
		key = randint(0,len(matrixKey)); gamma += str(key) + "/"			#Формирование гаммы
		shifr += matrixKey[(matrixKey.index(symbol) + key + len(matrixKey)//2)%len(matrixKey)]	
	return shifr,gamma
	
def Decr(cipher,gamma):		#Фунция расшифрования
	keys =  gamma.split('/')
	mes = ''
	for i, symbol in enumerate(cipher):
		if keys[i] != '':
			mes += matrixKey[(matrixKey.index(symbol) - int(keys[i])- len(matrixKey)//2)%len(matrixKey)]
	return mes
		

def input_text(choise):					#Функция, считывающая открытый текст и шифртекст из файлов
	if choise =="1":
		with open("Текст.txt", "r",encoding="utf-8") as openfile:
			mess = openfile.read()
		return mess		
	if choise =="2":
		with open("Шифртекст.txt", "r",encoding="utf-8") as openfile:
			mess = openfile.read()
		with open("key_Shenon.txt", "r",encoding="utf-8") as keyfile:
			gamma = keyfile.read()
		return mess, gamma
	
	

def out_cipher(cipher,gamma):					#Функция вывода шифртекста и гаммы в файлы
	with open("Шифртекст.txt", "w",encoding="utf-8") as openfile:
		openfile.write(cipher)
	with open("key_Shenon.txt", "w",encoding="utf-8") as keyfile:
		keyfile.write(gamma)


while 1:
	choise=input("[0] : Выход\n[1] : Шифрование\n[2] : Расшифрование(Перед расшифрованием необходимо скопировать гамму в файл 'key_Vernam.txt')\n Выбор - ")	#Определяет меню выбора				
	if choise=="1":							#Шифрование	
		encdata,key_vector=Encr(input_text(choise))
		out_cipher(encdata,key_vector)
		print("Зашифрованный текст = \n",encdata,"\n\nГамма = \n",key_vector,end="\n\n")
	elif choise=="2":						#Расшифрование
		cipher,gamma=input_text(choise)
		decrdata=Decr(cipher,gamma)
		print("Расшифрованный текст = \n",decrdata,end="\n\n")
	elif choise == "0":						#Выход
		sys.exit(0)
	else:									#Неправильный ввод пункта меню
		print("Выберите варианты меню")
		continue

