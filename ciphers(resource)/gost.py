import random
import hashlib
from gost_ec import ECPoint


class DSGOST:
    # q - целочисленное, порядок точки P
    # p_x, p_y - целочисленное, координаты точки Р
	# a, b - целочисленные, коэффициенты ЭК
	# p - целочисленное, модуль ЭК
    def __init__(self, p, a, b, q, p_x, p_y):				
        self.p_point = ECPoint(p_x, p_y, a, b, p)
        self.q = q
        self.a = a											
        self.b = b
        self.p = p											

    # Генерация пары ключей
    def gen_keys(self):
        d = random.randint(1, self.q - 1)
        q_point = d * self.p_point
        return d, q_point

    # Функция создания подписи 
	# message - целочисленное, сообщение
	# private_key - целочисленное, секретный ключ
    def sign(self, message, private_key, k=0):
        e = message % self.q								
        if e == 0:											
            e = 1
        if k == 0:
            k = random.randint(1, self.q - 1)		# случайное k, если оно не задано изначально
        r, s = 0, 0
        while r == 0 or s == 0:
            c_point = k * self.p_point
            r = c_point.x % self.q
            s = (r * private_key + k * e) % self.q
        return r, s

    # Проверка подписи
	# message - целочисленное
	# sign - кортеж (r,s)
	# public_key - класс ECPoint
    def verify(self, message, sign, public_key):
        e = message % self.q								    
        if e == 0:												
            e = 1												
        nu = ECPoint._mod_inverse(e, self.q)
        z1 = (sign[1] * nu) % self.q
        z2 = (-sign[0] * nu) % self.q
        c_point = z1 * self.p_point + z2 * public_key
        r = c_point.x % self.q
        if r == sign[0]:
            return print("R = ",r,"\nr=R\nПодпись верна",end="\n\n")
        return print("R = ",r,"\nr!=R\nПодпись неверна\n\n",end="\n\n")

#Функция, считывающая открытый текст из файла и возвращающая его хеш (SHA-256)
def input_text_and_hash():					
	with open("Текст.txt", "r",encoding="utf-8") as openfile:
		mess = openfile.read()
	hash_object = hashlib.sha256(mess.encode()) 
	hash_text= hash_object.hexdigest()
	hash_dec = int(hash_text,16)
	return hash_dec
	
	
def out_sign(sign):					#Функция сохранения подписи в файл
	line = str(sign[0]) + "," + str(sign[1])
	with open("Подпись.txt", "w",encoding="utf-8") as openfile:
		openfile.write(line)
		
def input_sign():					#Функция ввода подписи из файла				
	with open("Подпись.txt", "r",encoding="utf-8") as openfile:
		line = openfile.read().split(",")
		for i in range(len(line)):
			line[i]=int(line[i])
	return line



# Значения задавать перед вычислением или проверкой ЭЦП  
# По умолчанию,заданы значения из ГОСТа
p = 57896044618658097711785492504343953926634992332820282019728792003956564821041			# Модуль ЭК
a = 7																						# коэффициенты а и b ЭК
b = 43308876546767276905765904595650931995942111794451039583252968842033849580414			
x = 2																						# координата x точки P
y = 4018974056539037503335449422937059775635739389905545080690979365213431566280			# координата y точки P
q = 57896044618658097711785492504343953927082934583725450622380973592137631069619			# порядок подгруппы точки P

key = 55441196065363246126355624130324183196576709222340016572108097750006097525544			# Личный ключ подписи d (поставить 0 для генерации)
k = 53854137677348463731403841147996619241504003434302020712960838528893196233395			# Случайное число k (поставить = 0 для генерации)

q_x = 57520216126176808443631405023338071176630104906313632182896741342206604859403			# координаты точки Q для проверки подписи (обязательно поменять при генерации нового d)
q_y = 17614944419213781543809391949654080031942662045363639260709847859438286763994

gost = DSGOST(p, a, b, q, x, y)

while 1:
	choise=input("[0] : Выход\n[1] : Вычисление подписи\n[2] : Проверка подписи\n Выбор - ")	#Определяет меню выбора				
	if choise=="1":							#Вычисление подписи	
		hash = input_text_and_hash()
		print("Хеш = ",hash)
		if key == 0:
			key,q_point = gost.gen_keys(key)
			q_x = q_point.x 
			q_y = q_point.y
			print ("d = ", key)
		signature=gost.sign(hash,key,k)
		out_sign(signature)
		print ("Qx = ", q_x)
		print ("Qy = ", q_y)
		print("Подпись(r,s) = \n",signature,end="\n\n")	
	elif choise=="2":						#Проверка подписи
		hash = input_text_and_hash()
		signature= input_sign()
		public_key = ECPoint(q_x, q_y, a, b, p)
		gost.verify(hash, signature, public_key) 
	elif choise == "0":						#Выход
		sys.exit(0)
	else:									#Неправильный ввод пункта меню
		print("Выберите варианты меню")
		continue

